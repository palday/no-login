# No Login #

Sometimes it's necessary to block a user from logging in but not completely remove their account. One of the easier ways to do this is to redirect their login shell to a short tail script to dump a message. This is a collection of messages.

Inspired by [this page](http://linux.about.com/od/lsa_guide/a/gdelsa74.htm).